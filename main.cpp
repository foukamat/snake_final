#include "snake.hpp"
#include "menu.hpp"

int main(int argc, char **argv)
{
    
    while(1)
    {
        Menu menu;
        menu.start();
        if(menu.getstate()==1){
            Snake snake;
            snake.gameloop();
        }else{
            break;
        }

    }
    return 0;

};
