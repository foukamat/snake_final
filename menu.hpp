#include <SDL2/SDL.h>

class Menu {
    public:
        Menu();
        ~Menu();
        int getstate();
        void start();
    private:
        SDL_Window *window;
        SDL_Renderer *renderer;
        SDL_Texture *sprites;
        int WIDTH = 320;
        int HEIGHT = 364;
        int state = 0;

};