#include "menu.hpp"

Menu::Menu()
{
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN, &window, &renderer);
    auto surface = SDL_LoadBMP("menu.bmp");
    sprites = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    

};
void Menu::start()
{
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, sprites, NULL, NULL);    
    SDL_RenderPresent(renderer);
    
    for(bool end = false; !end;)
    {
        SDL_Event e;
        if(SDL_PollEvent(&e))
        {
            switch(e.type)
            {
                case SDL_KEYDOWN:
                {
                    switch (e.key.keysym.sym)
                    {
                        case SDLK_SPACE:
                        {
                            state = 1;
                            end = true;
                            break;
                        }
                        case SDLK_ESCAPE:
                            state = 0;
                            end = true;
                            break;
                        }
                    break;
                }
                case SDL_QUIT:
                    end = true;
                    break;

            }
        }
    }
};
Menu::~Menu()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
};
int Menu::getstate(){
    return state;
};