#include "snake.hpp"
#include <iostream>


Snake::Snake()
{
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN, &window, &renderer);

    //SDL_SetWindowPosition(window, 65, 126);

    auto surface = SDL_LoadBMP("sprites.bmp");
    sprites = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    SDL_SetRenderDrawColor(renderer, 117,127,109, 0.5);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    tiles.push_back(std::make_pair(3,1));
    tiles.push_back(std::make_pair(2,1));
    tiles.push_back(std::make_pair(1, 1));

};
Snake::~Snake()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
};
void Snake::draw()
{
    SDL_Rect texture;
    texture.y = 0;
    texture.w = 64;
    texture.h = 64;
    SDL_Rect destination;
    destination.w = 64;
    destination.h = 64;
    for(auto iter = std::begin(tiles); iter != std::end(tiles);iter++){
        int rotation = 0;
        auto &coords = *iter;
        auto &coordsFront = *(iter-1);
        auto &coordsBack = *(iter+1);
        if(iter == std::begin(tiles))
        {
            texture.x = Head;
            if(d.x == 1){
              rotation = 0;
            }
            if(d.x == -1){
              rotation = 180;
            }
            if(d.y == 1){
              rotation = 90;
            }
            if(d.y == -1){
              rotation = 270;
            }
        }
        else if (iter + 1 == std::end(tiles))
        {
          texture.x = Tail;
          if(coordsFront.first-coords.first == -1
          || coordsFront.first-coords.first == TILESWIDTH-1){
            rotation = 180;
          }
          else if(coordsFront.first-coords.first == 1
          || coordsFront.first-coords.first == -TILESWIDTH+1){
            rotation = 0;
          }
          else if(coordsFront.second-coords.second == -1
          || coordsFront.second-coords.second == TILESHEIGHT-1){
            rotation = 270;
          }
          else if(coordsFront.second-coords.second == 1
          || coordsFront.second-coords.second == -TILESHEIGHT+1){
            rotation = 90;
          }
        }
        else{
            bool up = false, down = false, left = false, right = false;
            
            if((coordsBack.first-coords.first == -1) 
            || (coordsFront.first-coords.first == -1)
            || (coordsBack.first-coords.first == TILESWIDTH-1) 
            || (coordsFront.first-coords.first == TILESWIDTH-1)){
              left = true;
            }
            if((coordsBack.first-coords.first == 1) 
            || (coordsFront.first-coords.first == 1)
            || (coordsBack.first-coords.first == -TILESWIDTH+1) 
            || (coordsFront.first-coords.first == -TILESWIDTH+1)){
              right = true;
            }
            if((coordsBack.second-coords.second == 1) 
            || (coordsFront.second-coords.second == 1)
            || (coordsBack.second-coords.second == -TILESHEIGHT+1)
            || (coordsFront.second-coords.second == -TILESHEIGHT+1)){
              down = true;
            }
            if(coordsBack.second-coords.second == -1 
            || (coordsFront.second-coords.second == -1)
            || (coordsBack.second-coords.second == TILESHEIGHT-1)
            || (coordsFront.second-coords.second == TILESHEIGHT-1)){
              up = true;
            }
            if(left && right){
              texture.x = Body;
              rotation = 0;
            }
            else if (up && down){
              texture.x = Body;
              rotation = 90;
            }
            else if(up && right ){
              texture.x = Lbody;
              rotation = 270;
            }
            else if(up && left){
              texture.x = Lbody;
              rotation = 180;
            }
            else if(left && down){
              texture.x = Lbody;
              rotation = 90;
            }
            else if (down && right){
              texture.x = Lbody;
              rotation = 0;
            }
        }
        destination.x = 64*coords.first;
        destination.y = 64*coords.second;
        SDL_RenderCopyEx(renderer, sprites, &texture, &destination, rotation, nullptr, SDL_FLIP_NONE);
    }
    //jablko
    texture.x = 256;
    destination.x = applex*64;
    destination.y = appley*64;
    SDL_RenderCopyEx(renderer, sprites, &texture, &destination, 0, nullptr, SDL_FLIP_NONE);
    SDL_RenderPresent(renderer);

};
void Snake::gameloop()
{
  spawnapple();
  unsigned int lastTime = 0, currentTime;
  for (bool end = false; !end;)
  {
    SDL_Event e;
    if (SDL_PollEvent(&e))
    {
      switch (e.type)
      {
        case SDL_KEYDOWN:
        {
          switch(e.key.keysym.sym)
          {
            case SDLK_UP:
              changedirection(0,-1);
              break;
            case SDLK_DOWN:
              changedirection(0,1);
              break;
            case SDLK_LEFT:
              changedirection(-1,0);
              break;
            case SDLK_RIGHT:
              changedirection(1,0);
              break;
            case SDLK_ESCAPE:
              end = true;
              break;
          }
          break;
        }
        case SDL_QUIT:
        {
          end = true;
          break;
        }
      }
    }
    
    SDL_RenderClear(renderer);
    currentTime=SDL_GetTicks()/180;
    if(currentTime > lastTime){
      auto head = tiles.front();
      head.first+=d.x;
      if(head.first > (WIDTH-64)/64){
        head.first = 0;
      }
      else if(head.first < 0){
        head.first = (WIDTH - 64)/64;
      }
      head.second+=d.y;
      if(head.second > (HEIGHT-64)/64){
        head.second = 0;
      }
      else if(head.second < 0){
        head.second = (HEIGHT - 64)/64;
      }
      if(collision(head.first, head.second)){
        end = true;
      }
      if(head.first == applex && head.second == appley){
        spawnapple();
      }
      else{
        tiles.pop_back();
      }
      tiles.push_front(head);
      lastTime = currentTime;
      draw();
      
    }
  }
};
bool Snake::collision(int x, int y){
  for(auto iter = std::begin(tiles); iter < std::end(tiles);iter++){
    auto &coords = *iter;
    if(x == coords.first && y == coords.second){
      return true;
    }
  }
  return false;
};
void Snake::changedirection(int x, int y){
  d.x = x;
  d.y = y;
}
void Snake::spawnapple(){
  int x;
  int y;
  for(bool done = false; !done;){
    x = rand() % TILESWIDTH;
    y = rand() % TILESHEIGHT;
    if(!collision(x,y)){
      applex = x;
      appley = y;
      done = true;
    }
  }
}