snake: main.o snake.o menu.o
	g++ $^ -o snake -lSDL2

%.o: %.cpp
	g++ -c $<

run:
	./snake

clean:
	rm *.o snake