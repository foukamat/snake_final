                            |=======================|
                            | SEMESTRÁLNÍ PRÁCE PPC |
                            | MATĚJ FOUKAL 2021     |
                            |=======================|

Jako semestrální práci jsem si vybral hru "Snake", která byla původně vydaná pro Nokii 3310.
Nesnažil jsem se hru napodobit dopodrobna, pouze použít její koncept.

Pro vytvoření hry jsem použil knihovnu SDL2 a je tedy potřeba před sestavením.

Hra testována na Ubuntu 20.04.2 LTS, měla by však být zcela funkční i na Windows.

SPUŠTĚNÍ HRY 
    1. sestavit hru
        > make
    2. spustit hru
        > make run

OVLÁDÁNÍ
    Po spuštění se dostanete do menu, zde máte 2 možnosti.
        - spuštění hry pomocí klávesy 'SPACE'
        - opuštění hry pomocí klávesy 'ESCAPE'
    Jakmile se dostanete do hry, budete moci pomocí šipek hada ovládat,
    vaším cílem je sníst co nejvíce jablek (v mé hře vypadají spíš jako rajčata)
    a tím dosáhnout co největší délky hada.
    Ve hře máte taky možnost zmáčknout 'ESC' pro navrácení do menu.

V přiloženém videu "demonstrace.webm" můžete vidět sestavení i běh programu.



známé chyby:
    Někdy se včas neobnovují snímky, pravděpodobně je to způsobené VSync, ale řešení bylo nad moje schopnosti. 
    Chyba se mi stala pouze jednou a po restartu už hra fungovala normálně.