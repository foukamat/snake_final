#include <deque>
#include <SDL2/SDL.h>

struct direction {
  int x = 1;
  int y = 0;
};
class Snake 
{
    public:
        Snake();
        ~Snake();
        void draw();
        void gameloop();
        void spawnapple();
        void changedirection(int x, int y);
        bool collision(int x, int y);
        
        const int WIDTH = 640;
        const int HEIGHT = 640;

        const int TILESWIDTH = WIDTH/64;
        const int TILESHEIGHT = HEIGHT/64;
    private:
        SDL_Window *window;
        SDL_Renderer *renderer;
        SDL_Texture *sprites;
        const int Body = 0;
        const int Lbody = 64;
        const int Head = 128;
        const int Tail = 192;
        std::deque<std::pair<int, int> > tiles;
        direction d;
        int applex;
        int appley;
        //int gamespeed = 250;

};